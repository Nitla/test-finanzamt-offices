package com.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity {
    String jsonObj = null;

    String longtitude = null;
    String latitude = null;

    String officeName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // get one office json object string from previous activity
        jsonObj = getIntent().getStringExtra("json");

        setupToolBar();

        showOfficeInfo();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //back button pressed so go back
            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Show selected Ofice's detailed info
    private void showOfficeInfo() {
        TextView name = findViewById(R.id.name);
        TextView address = findViewById(R.id.address);
        TextView opening_hours = findViewById(R.id.opening_hours);
        TextView phone = findViewById(R.id.phone);

        try {
            JSONObject jsObj = new JSONObject(jsonObj);

            String zip = jsObj.getString("DisPlz");
            String city = jsObj.getString("DisOrt");
            String street = jsObj.getString("DisStrasse");

            String imgUrl = jsObj.getString("DisFotoUrl");

            // get the Image of the office from web
            fetchImage(imgUrl);

            latitude = jsObj.getString("DisLatitude");
            longtitude = jsObj.getString("DisLongitude");
            officeName = jsObj.getString("DisNameLang");

            String disName = officeName;
            String disOpenHours = jsObj.getString("DisOeffnung");
            String disPhone = jsObj.getString("DisTel");
            String disAddress = zip + " " + city + ", " + street;

            //show values accordingly
            name.setText(disName);
            address.setText(disAddress);
            opening_hours.setText(disOpenHours);
            phone.setText(disPhone);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fetchImage(String imgUrl) {
        // ImageLoader is a library used to fetch images from web
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageView img = findViewById(R.id.img);
        imageLoader.loadImage(imgUrl, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img.setImageBitmap(loadedImage);

                // hide the progressBar as we already
                View imgProgressBar = findViewById(R.id.imgProgressBar);
                imgProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                super.onLoadingFailed(imageUri, view, failReason);

                View imgProgressBar = findViewById(R.id.imgProgressBar);
                imgProgressBar.setVisibility(View.GONE);

                Toast.makeText(DetailActivity.this,"Could not get the image!",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    // show back button on actionbar
    void setupToolBar(){
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // Start MapActivity
    public void showLocationOnMap(View v) {
        Intent i = new Intent(DetailActivity.this, MapActivity.class);

        // send lat, lng and office name to the MapActivity
        i.putExtra("longtitude", longtitude);
        i.putExtra("latitude", latitude);
        i.putExtra("officeName", officeName);

        startActivity(i);
    }

}
