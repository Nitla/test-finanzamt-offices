package com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        setupToolBar();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Get lat, ln and office name from previous Activity
        String latitude = getIntent().getStringExtra("latitude");
        String longtitude = getIntent().getStringExtra("longtitude");
        String officeName = getIntent().getStringExtra("officeName");

        // Add the marker. Parse lat and ln to double as they came as String
        LatLng officeMarker =
                new LatLng(Double.parseDouble(latitude), Double.parseDouble(longtitude));

        // put the office name to the marker
        googleMap.addMarker(new MarkerOptions().position(officeMarker).title(officeName));
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(officeMarker.latitude, officeMarker.longitude), 17f));

    }

    void setupToolBar(){
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            //back button pressed so go back
            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
