package com.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    final int REQUEST_WRITE_EXTERNAL_STORAGE = 0;

    final String TAG = "MainActivity";

    final String API = "https://service.bmf.gv.at/Finanzamtsliste.json";

    final String DATA_FILENAME = "AppDemo.json";

    Callback startPopulate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set actionbar label
        setupToolBar();

        // set the callback needed after fetching is done
        startPopulate = this::populateScrollView;

        // check storage permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
            return;
        }

        // if we already have access to the storage, fetch JSON api from web
        fetchCentersList();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // If request is cancelled, the result arrays are empty.
        if (requestCode == REQUEST_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted

                fetchCentersList();
            } else {
                // permission denied

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.YesNoDialog);
                LayoutInflater inflater = getLayoutInflater();
                builder.setView(inflater.inflate(R.layout.dialog_yes_no, null));

                final AlertDialog yesNoDialog = builder.create();
                yesNoDialog.show();

                View yesBtn = yesNoDialog.findViewById(R.id.dialog_yes);
                assert yesBtn != null;
                yesBtn.setTag(yesNoDialog);

                View noBtn = yesNoDialog.findViewById(R.id.dialog_no);
                assert noBtn != null;
                noBtn.setTag(yesNoDialog);

            }
        }
    }

    // Set Toolbar Title manually because the Label used in Manifest
    // would change the Launcher label as well
    void setupToolBar(){
        Objects.requireNonNull(getSupportActionBar()).setTitle("Finanzamt Offices");
    }

    // fetch JSon office list from web
    void fetchCentersList() {
        new Thread(() -> {
            try {
                URL url = new URL(API);

                // Read all the JSON returned by the server encoded in ISO_8859_1
                BufferedReader in =
                        new BufferedReader(
                                new InputStreamReader(url.openStream(),
                                        StandardCharsets.ISO_8859_1));
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = in.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                in.close();

                // try to write the JSon string to a file
                writeToFile(stringBuilder.toString());
            } catch (MalformedURLException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());

                runOnUiThread(() ->
                        Toast.makeText(MainActivity.this,
                                "No internet connection!", Toast.LENGTH_LONG).show());

                // no internet. we try to read from phone memory
                populateScrollView();
            }
        }).start();
    }

    // read jSon text file
    String readFile(){
        File dataFile = new File(this.getExternalFilesDir(null), DATA_FILENAME);

        String line = null;

        if (dataFile.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream (dataFile);

                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuilder stringBuilder = new StringBuilder();

                while ( (line = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(line);
                }

                fileInputStream.close();

                line = stringBuilder.toString();

                bufferedReader.close();
            } catch(FileNotFoundException ex) {
                Log.d(TAG, ex.getMessage());
            } catch(IOException ex) {
                Toast.makeText(this, "File empty!", Toast.LENGTH_LONG).show();
                Log.d(TAG, ex.getMessage());
            }
        }

        return line;
    }

    // write jSon string to a text file
    private void writeToFile(String data) {
        File dataFile = new File(this.getExternalFilesDir(null), DATA_FILENAME);

        try {
            Writer out =
                    new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream(dataFile.getPath()), StandardCharsets.UTF_8));

            out.append(data);

            out.flush();
            out.close();

            //saving finished. start populating Scrollview
            startPopulate.callback();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // add all Office items to the scrollview
    void populateScrollView() {
        File dataFile = new File(this.getExternalFilesDir(null), DATA_FILENAME);

        if (dataFile.exists()) {
            try {
                ArrayList<OfficeItem> list = sortList(readFile());

                for (OfficeItem item: list) {
                    runOnUiThread(() -> {
                        showOneOfficeItem(item);
                    });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            runOnUiThread(() ->
                    Toast.makeText(MainActivity.this,
                            "File not found. Couldnt read any data stored!",
                            Toast.LENGTH_LONG).show());
        }
    }

    // add one Office view to the Scrollview
    void showOneOfficeItem(OfficeItem item){
        ViewGroup scv = findViewById(R.id.scv);

        // inflate and prepare an Office view from office_item layout
        LayoutInflater inflater = LayoutInflater.from(this);
        View inflatedLayout = inflater.inflate(R.layout.office_item, scv, false);
        scv.addView(inflatedLayout);

        // put the JSON string to the tag of parent view
        // to reach and post it to details Activity
        inflatedLayout.setTag(item.jsObj);

        TextView title_txt = inflatedLayout.findViewById(R.id.title_txt);
        TextView subtitle_txt = inflatedLayout.findViewById(R.id.subtitle_txt);

        // set texts acordingly
        title_txt.setText(item.title);
        subtitle_txt.setText(item.subtitle);
    }

    // onClick of every office item from scrollview
    public void startDetailsActivity(View v) {
        System.out.println(v.getTag());

        Intent i = new Intent(MainActivity.this, DetailActivity.class);

        // put json string in order to reach it from DetailActivity
        i.putExtra("json", String.valueOf(v.getTag()));

        startActivity(i);
    }

    //permission dialog Yes button
    public void dialogYesClicked(View v) {
        AlertDialog dialog = (AlertDialog) v.getTag();
        dialog.dismiss();

        // re ask for storage access permission
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    //permission dialog No button
    public void dialogNoClicked(View v) {
        AlertDialog dialog = (AlertDialog) v.getTag();
        dialog.dismiss();

        // access is not granted
        Toast.makeText(this, "The app has to get access to storage in order to work!",
                Toast.LENGTH_LONG).show();
    }

    // Sort the JSON string in ascending order according to zip codes
    ArrayList<OfficeItem> sortList(String data) throws JSONException {
        JSONArray jsArr = new JSONArray(data);

        ArrayList<OfficeItem> list = new ArrayList<>();

        for (int i = 0; i < jsArr.length(); i++) {
            JSONObject jsObj = jsArr.getJSONObject(i);

            String name = jsObj.getString("DisNameLang");
            String zip = jsObj.getString("DisPlz");
            String city = jsObj.getString("DisOrt");
            String street = jsObj.getString("DisStrasse");
            String subtitle =
                    "Zip: " + zip + "\n" +
                    "City: " + city + "\n" +
                    "Street: " + street;

            list.add(new OfficeItem(Integer.parseInt(zip), name, subtitle, jsObj.toString()));
        }

        Collections.sort(list);

        return list;
    }

    // a class to sort office items
    class OfficeItem implements Comparable{
        private int zip;
        private String title;
        private String subtitle;
        private String jsObj;

        OfficeItem(int zip, String title, String subtitle, String jsObj) {
            this.title = title;
            this.subtitle = subtitle;
            this.zip = zip;
            this.jsObj = jsObj;
        }

        @Override
        public int compareTo(Object o) {
            OfficeItem p = (OfficeItem) o;
            return this.zip - p.zip;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public int getZip() {
            return zip;
        }

        public void setZip(int zip) {
            this.zip = zip;
        }

        public String getJsObj() {
            return jsObj;
        }

        public void setJsObj(String jsObj) {
            this.jsObj = jsObj;
        }
    }

}
